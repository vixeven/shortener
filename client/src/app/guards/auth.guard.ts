import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ApiService } from '../services/api.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private api: ApiService,
    private router: Router
  ){}

  canActivate(){
    if(!this.api.loggedIn()){
      return true;
    } else {
      this.router.navigate(['/'])
      return false
    }
  }
}
