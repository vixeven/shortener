import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';

import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {

  authToken: any;
  user: any;

  constructor(private http:Http) { }

  
  register(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/users/register', user, {headers: headers});
  }

  login(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/users/login', user, {headers: headers});
  }

  getURLs() {
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization',this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get('/link/my', {headers: headers})
      .map(res => res.json())
  }

  loadToken() {
    this.authToken = localStorage.getItem('id_token')
  }

  loggedIn() {
    return tokenNotExpired('id_token');
  }

  shortURL(link) {
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization',this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post('/link/add', link, {headers: headers});
  }

  anonim(link) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('/link/anonim', link, {headers: headers});
  }

  saveToken(token, username){
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', username);
    this.authToken = token;
    this.user = username;
  }

  logout(){
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

}
