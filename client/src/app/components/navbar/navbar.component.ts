import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { MdSnackBar } from '@angular/material';

import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  constructor(
    private api: ApiService,
    private router: Router,
    private snackBar: MdSnackBar,
  ) { }

  logout() {
    this.api.logout();
    this.snackBar.open("Yo're logged out.", "Close", { duration: 3000 });
    this.router.navigate(['/login']);
  }

}
