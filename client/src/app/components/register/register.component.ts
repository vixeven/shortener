import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { ApiService } from '../../services/api.service';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const NAME_REGEX = /^\s*([A-Z\s])([a-z\s]){1,30}([A-Z\s])([a-z\s]){1,30}\s*$/;
const PASSWORD_REGEX = /^.{6,36}$/;
const USERNAME_REGEX = /^[a-zA-Z]{4,20}$/;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name: String;
  email: String;
  password: String;
  username: String;

  constructor(
    private snackBar: MdSnackBar,
    private api: ApiService,
    private router: Router
  ) {}

  toast(message: string) {
    this.snackBar.open(message, "Close", {
      duration: 3000
    });
  }

  ngOnInit() {
  }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(EMAIL_REGEX)]);

  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(NAME_REGEX)]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(PASSWORD_REGEX)]);
  
  usernameFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(USERNAME_REGEX)
  ]) 
  
  onRegister() {
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password
  }
    this.api.register(user).subscribe(
      data => {
        if (data.ok) {
          this.toast("User has succesfully created.");
        } else {
          this.toast("Something went wrong.");
        }
        this.router.navigate(['/login']);
      },
      error => {
        console.log("WTF")
      }
    )
  }
}
