const mongoose = require("mongoose")
const bcrypt = require("bcryptjs")
const shortid = require('shortid')

const config = require("../config/database")

const LinkSchema = mongoose.Schema({
    URL: { type: String, required: true },
    acceses: { type: Number, required: true, default: 0, },
    author: { type: String, required: true, default: 'anonim' },
    created: { type: Date, required: true, default: Date.now },
    key: { type: String, default : shortid.generate, required: true }
})

const Link = module.exports = mongoose.model('Link', LinkSchema)

module.exports.getLinkById = function(id, callback){
    Link.findById(id, callback)
}

module.exports.addLink = function(link, callback){
    link.save(callback);
}

module.exports.getLinks = function(author, callback){
    const query = {author: author}
    Link.find(query, callback)
}

module.exports.getURLbyKey = function(key, callback){
    const query = {key: key}
    Link.findOne(query, callback)
}

