# Server

This project was created with NodeJS 8.2.1, ExpressJS, mongoose and manny other modern tehnologies.

## Development server

Run `npm install` for install all dependecies.
Now run `app.js` and after, navigate to `http://localhost:3000/`. The app will automatically reload if you run project with nodemon.

## MongoDB

For run this project you will need to have MongoDB installed. See [MongoDB.org](https://mongodb.org) for more information and resources.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
