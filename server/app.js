const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const cors = require('cors')
const passport = require('passport')
const mongoose = require('mongoose')

const users = require("./controllers/users")
const link = require("./controllers/links")

const port = 3000

const app = express()

// Mongo Connection
const config = require('./config/database')

mongoose.connect(config.database)

mongoose.connection.on('connected', () => {
    console.log("Connected to database "+config.database)
});

/* Alternative way to do connection
mongoose.connect(config.database).then(
    () => { console.log("Connected to database "+config.database) },
    err => { console.log(err) }
)
*/

// Allows request from other sources
app.use(cors())

/*    Set Static Folder    */
app.use(express.static(path.join(__dirname, 'public')))

app.use(bodyParser.json())

// Passport Initialisation
app.use(passport.initialize())
app.use(passport.session())
require('./config/passport')(passport);


/*  API   */
app.use('/users', users)
app.use('/link', link)

/*  Angular2 App   */
app.get(['/', '/login', '/register'], (req, res) => {
    res.sendFile(path.join(__dirname, "public/index.html"))
})

const Link = require('./models/links')
/*  REDIRECT FROM KEY   */
app.get('/:key', (req, res) => {
    console.log(Date.now())
    Link.getURLbyKey(req.params.key, (err, link) => {
        if (err)
            throw err;
        if (link){
            //console.log(Date.now())
            res.redirect(link.URL)
        }
        else
            res.send("Invalid key.")
    })
})

app.use((err, req, res, next) => {
  if (err) {
    if (err.name === "MongoError") {
      if (err.code === 11000) res.status(406).send("Duplicate key error");
    } else res.status(err.status || 400).send(err.message);
  } else next();
});


app.listen(process.env.PORT || port, () => {
    console.log('Server stared '+port)
})