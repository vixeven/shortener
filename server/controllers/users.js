const CreateError = require("http-errors")
const express = require('express')

const router = express.Router()

const User = require('../models/user')
const passport = require('passport')
const jwt = require('jsonwebtoken')
const config = require('../config/database')

router

.post('/register', (req, res, next) => {
    let newUser = new User({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    })

    User.addUser(newUser, (err, user) => {
        if (err)
            next(CreateError(400, err))
        res.status(200).send("User has succesfuly registred.")
    })
})

.post('/login', (req, res, next) => {
    const username = req.body.username
    const password = req.body.password

    User.getUserByUsername(username, (err, user) => {
        if (err) next(CreateError(400, err))
        if (!user)
            next(CreateError(404, "User not found."))
        else {

        User.comparePasswords(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch){
                const token = jwt.sign(user, config.secret, {
                    expiresIn: 604800
                })
                res.json({
                    token: "JWT "+token,
                    username: user.username
                })
            } else {
                next(CreateError(401, "Incorrect password."))
            }
        })
        }
    })
})

.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    res.json({user: req.user})
})

module.exports = router